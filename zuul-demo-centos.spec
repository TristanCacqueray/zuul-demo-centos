Summary: A demo
Name: zuul-demo-centos
Version: 1.0.0
Release: centos
License: Apache-2
URL: https://pagure.io/zuul-distro-jobs
Source0: HEAD.tgz

%description
Just a demo

%prep

%build

%install


%files
%defattr(-,root,root,-)
%doc


%changelog
* Thu Mar  4 2021 Tristan Cacqueray <tdecacqu@redhat.com> - demo-centos
- Initial build.
